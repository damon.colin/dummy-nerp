package com.craft.nerp.invoice.infrastructure.secondary;

import com.craft.nerp.invoice.domain.Line;
import com.craft.nerp.invoice.infrastructure.secondary.SerializableLine.SerializableLineBuilder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import java.math.BigDecimal;

@JsonDeserialize(builder = SerializableLineBuilder.class)
class SerializableLine {

  private final String label;
  private final int quantity;
  private final BigDecimal unitPrice;

  private SerializableLine(SerializableLineBuilder builder) {
    label = builder.label;
    quantity = builder.quantity;
    unitPrice = builder.unitPrice;
  }

  static SerializableLine from(Line line) {
    return new SerializableLineBuilder()
      .label(line.label().value())
      .quantity(line.quantity().value())
      .unitPrice(line.unitPrice().value())
      .build();
  }

  Line toDomain() {
    return Line.builder().label(label).quantity(quantity).unitPrice(unitPrice);
  }

  public String getLabel() {
    return label;
  }

  public int getQuantity() {
    return quantity;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  @JsonPOJOBuilder(withPrefix = "")
  static class SerializableLineBuilder {

    private String label;
    private int quantity;
    private BigDecimal unitPrice;

    public SerializableLineBuilder label(String label) {
      this.label = label;

      return this;
    }

    public SerializableLineBuilder quantity(int quantity) {
      this.quantity = quantity;

      return this;
    }

    public SerializableLineBuilder unitPrice(BigDecimal unitPrice) {
      this.unitPrice = unitPrice;

      return this;
    }

    public SerializableLine build() {
      return new SerializableLine(this);
    }
  }
}
