package com.craft.nerp.invoice.infrastructure.secondary;

import com.craft.nerp.customer.infrastructure.primary.JavaCustomers;
import com.craft.nerp.invoice.domain.recipient.Recipient;
import com.craft.nerp.invoice.domain.recipient.RecipientFirstname;
import com.craft.nerp.invoice.domain.recipient.RecipientId;
import com.craft.nerp.invoice.domain.recipient.RecipientLastname;
import com.craft.nerp.invoice.domain.recipient.RecipientName;
import com.craft.nerp.invoice.domain.recipient.RecipientsRepository;
import com.craft.nerp.invoice.domain.recipient.address.RecipientAddress;
import com.craft.nerp.invoice.domain.recipient.address.RecipientCity;
import com.craft.nerp.invoice.domain.recipient.address.RecipientPostalCode;
import com.craft.nerp.invoice.domain.recipient.address.RecipientStreet;
import com.craft.nerp.shared.customer_identity.domain.CustomerIdentity;
import com.craft.nerp.shared.error.domain.Assert;
import java.util.Optional;
import java.util.function.Function;
import org.springframework.stereotype.Repository;

@Repository
class JavaRecipientsRepository implements RecipientsRepository {

  private final JavaCustomers customers;

  public JavaRecipientsRepository(JavaCustomers customers) {
    this.customers = customers;
  }

  @Override
  public Optional<Recipient> get(RecipientId recipient) {
    Assert.notNull("recipient", recipient);

    return customers.get(recipient.value()).map(toRecipient());
  }

  private Function<CustomerIdentity, Recipient> toRecipient() {
    return customer -> new Recipient(buildName(customer), buildAddress(customer));
  }

  private RecipientName buildName(CustomerIdentity customer) {
    return new RecipientName(new RecipientFirstname(customer.firstname()), new RecipientLastname(customer.lastname()));
  }

  private RecipientAddress buildAddress(CustomerIdentity customer) {
    return RecipientAddress
      .builder()
      .street(new RecipientStreet(customer.street()))
      .postalCode(new RecipientPostalCode(customer.postalCode()))
      .city(new RecipientCity(customer.city()));
  }
}
