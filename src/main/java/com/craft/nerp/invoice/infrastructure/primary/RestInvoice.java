package com.craft.nerp.invoice.infrastructure.primary;

import com.craft.nerp.invoice.domain.Invoice;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.UUID;

@Schema(name = "Invoice", description = "An invoice")
class RestInvoice {

  private final UUID id;
  private final RestRecipient recipient;
  private final Collection<RestLine> lines;
  private final BigDecimal total;

  private RestInvoice(RestInvoiceBuilder builder) {
    id = builder.id;
    recipient = builder.recipient;
    lines = builder.lines;
    total = builder.total;
  }

  public static RestInvoice from(Invoice invoice) {
    return new RestInvoiceBuilder()
      .id(invoice.id().value())
      .recipient(RestRecipient.from(invoice.recipient()))
      .lines(invoice.lines().stream().map(RestLine::from).toList())
      .total(invoice.total().value())
      .build();
  }

  @Schema(description = "ID of this invoice", requiredMode = RequiredMode.REQUIRED)
  public UUID getId() {
    return id;
  }

  @Schema(description = "Recipient of this invoice", requiredMode = RequiredMode.REQUIRED)
  public RestRecipient getRecipient() {
    return recipient;
  }

  @Schema(description = "Lines in this invoice", requiredMode = RequiredMode.REQUIRED)
  public Collection<RestLine> getLines() {
    return lines;
  }

  @Schema(description = "Total of this invoice", requiredMode = RequiredMode.REQUIRED)
  public BigDecimal getTotal() {
    return total;
  }

  private static class RestInvoiceBuilder {

    private UUID id;
    private RestRecipient recipient;
    private Collection<RestLine> lines;
    private BigDecimal total;

    private RestInvoiceBuilder id(UUID id) {
      this.id = id;

      return this;
    }

    public RestInvoiceBuilder recipient(RestRecipient recipient) {
      this.recipient = recipient;

      return this;
    }

    public RestInvoiceBuilder lines(Collection<RestLine> lines) {
      this.lines = lines;

      return this;
    }

    public RestInvoiceBuilder total(BigDecimal total) {
      this.total = total;

      return this;
    }

    public RestInvoice build() {
      return new RestInvoice(this);
    }
  }
}
