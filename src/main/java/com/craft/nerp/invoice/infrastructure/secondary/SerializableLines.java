package com.craft.nerp.invoice.infrastructure.secondary;

import com.craft.nerp.invoice.domain.Lines;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Collection;

class SerializableLines {

  private final Collection<SerializableLine> lines;

  private SerializableLines(@JsonProperty("lines") Collection<SerializableLine> lines) {
    this.lines = lines;
  }

  public static SerializableLines from(Lines lines) {
    return new SerializableLines(lines.stream().map(SerializableLine::from).toList());
  }

  public Collection<SerializableLine> getLines() {
    return lines;
  }

  public Lines toDomain() {
    return new Lines(lines.stream().map(SerializableLine::toDomain).toList());
  }
}
