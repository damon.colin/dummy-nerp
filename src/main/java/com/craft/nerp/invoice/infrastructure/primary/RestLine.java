package com.craft.nerp.invoice.infrastructure.primary;

import com.craft.nerp.invoice.domain.Line;
import com.craft.nerp.invoice.infrastructure.primary.RestLine.RestLineBuilder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import java.math.BigDecimal;

@JsonDeserialize(builder = RestLineBuilder.class)
@Schema(name = "Line", description = "Line in an invoice")
class RestLine {

  private final String label;
  private final int quantity;
  private final BigDecimal unitPrice;

  private RestLine(RestLineBuilder builder) {
    label = builder.label;
    quantity = builder.quantity;
    unitPrice = builder.unitPrice;
  }

  Line toDomain() {
    return Line.builder().label(label).quantity(quantity).unitPrice(unitPrice);
  }

  static RestLine from(Line line) {
    return new RestLineBuilder().label(line.label().value()).quantity(line.quantity().value()).unitPrice(line.unitPrice().value()).build();
  }

  @NotBlank
  @Schema(description = "Label of this line", requiredMode = RequiredMode.REQUIRED)
  public String getLabel() {
    return label;
  }

  @Min(1)
  @Schema(description = "Quantity of this line", requiredMode = RequiredMode.REQUIRED)
  public int getQuantity() {
    return quantity;
  }

  @Min(0)
  @Schema(description = "Unit price of this line", requiredMode = RequiredMode.REQUIRED)
  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  @JsonPOJOBuilder(withPrefix = "")
  static class RestLineBuilder {

    private String label;
    private int quantity;
    private BigDecimal unitPrice;

    RestLineBuilder label(String label) {
      this.label = label;

      return this;
    }

    RestLineBuilder quantity(int quantity) {
      this.quantity = quantity;

      return this;
    }

    RestLineBuilder unitPrice(BigDecimal unitPrice) {
      this.unitPrice = unitPrice;

      return this;
    }

    RestLine build() {
      return new RestLine(this);
    }
  }
}
