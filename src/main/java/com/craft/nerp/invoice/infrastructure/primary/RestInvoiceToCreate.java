package com.craft.nerp.invoice.infrastructure.primary;

import com.craft.nerp.invoice.domain.InvoiceToCreate;
import com.craft.nerp.invoice.domain.Lines;
import com.craft.nerp.invoice.domain.recipient.RecipientId;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.Collection;
import java.util.UUID;

@Schema(name = "InvoiceToCreate", description = "Information to create an invoice")
class RestInvoiceToCreate {

  private final UUID recipient;
  private final Collection<@Valid RestLine> lines;

  RestInvoiceToCreate(@JsonProperty("recipient") UUID recipient, @JsonProperty("lines") Collection<RestLine> lines) {
    this.recipient = recipient;
    this.lines = lines;
  }

  public InvoiceToCreate toDomain() {
    Lines lines = new Lines(getLines().stream().map(RestLine::toDomain).toList());

    return new InvoiceToCreate(new RecipientId(recipient), lines);
  }

  @NotNull
  @Schema(description = "ID of the customer receiving this invoice", requiredMode = RequiredMode.REQUIRED)
  public UUID getRecipient() {
    return recipient;
  }

  @Valid
  @NotEmpty
  @Schema(description = "Lines for the invoice", requiredMode = RequiredMode.REQUIRED)
  public Collection<RestLine> getLines() {
    return lines;
  }
}
