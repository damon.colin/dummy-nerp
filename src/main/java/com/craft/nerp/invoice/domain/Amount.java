package com.craft.nerp.invoice.domain;

import com.craft.nerp.shared.error.domain.Assert;
import java.math.BigDecimal;
import java.math.RoundingMode;

public record Amount(BigDecimal value) {
  public static final Amount ZERO = new Amount(BigDecimal.ZERO);

  public Amount(BigDecimal value) {
    Assert.field("amount", value).notNull().positive();

    this.value = value.setScale(2, RoundingMode.HALF_UP);
  }

  public Amount times(Quantity quantity) {
    Assert.notNull("quantity", quantity);

    return new Amount(value().multiply(quantity.toBigDecimal()));
  }

  public Amount add(Amount other) {
    Assert.notNull("otherAmount", other);

    return new Amount(value().add(other.value()));
  }
}
