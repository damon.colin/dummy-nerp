package com.craft.nerp.invoice.domain.recipient.address;

import com.craft.nerp.shared.error.domain.Assert;

public record RecipientStreet(String value) {
  public RecipientStreet {
    Assert.field("street", value).notBlank().maxLength(255);
  }
}
