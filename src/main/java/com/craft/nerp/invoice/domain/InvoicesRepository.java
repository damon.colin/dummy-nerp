package com.craft.nerp.invoice.domain;

import java.util.Optional;

public interface InvoicesRepository {
  Optional<Invoice> get(InvoiceId invoiceId);

  void save(Invoice invoice);
}
