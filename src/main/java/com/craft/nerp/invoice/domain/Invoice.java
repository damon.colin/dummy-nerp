package com.craft.nerp.invoice.domain;

import com.craft.nerp.invoice.domain.recipient.Recipient;
import com.craft.nerp.shared.error.domain.Assert;

public class Invoice {

  private final InvoiceId id;
  private final Recipient recipient;
  private final Lines lines;

  private Invoice(InvoiceBuilder builder) {
    Assert.notNull("id", builder.id);
    Assert.notNull("recipient", builder.recipient);
    Assert.notNull("lines", builder.lines);

    id = builder.id;
    recipient = builder.recipient;
    lines = builder.lines;
  }

  public static InvoiceIdBuilder builder() {
    return new InvoiceBuilder();
  }

  public Amount total() {
    return lines.total();
  }

  public InvoiceId id() {
    return id;
  }

  public Recipient recipient() {
    return recipient;
  }

  public Lines lines() {
    return lines;
  }

  private static class InvoiceBuilder implements InvoiceIdBuilder, InvoiceRecipientBuilder, InvoiceLinesBuilder {

    private InvoiceId id;
    private Recipient recipient;
    private Lines lines;

    @Override
    public InvoiceRecipientBuilder id(InvoiceId id) {
      this.id = id;

      return this;
    }

    @Override
    public InvoiceLinesBuilder recipient(Recipient recipient) {
      this.recipient = recipient;

      return this;
    }

    @Override
    public Invoice lines(Lines lines) {
      this.lines = lines;

      return new Invoice(this);
    }
  }

  public interface InvoiceIdBuilder {
    InvoiceRecipientBuilder id(InvoiceId id);
  }

  public interface InvoiceRecipientBuilder {
    InvoiceLinesBuilder recipient(Recipient recipient);
  }

  public interface InvoiceLinesBuilder {
    Invoice lines(Lines lines);
  }
}
