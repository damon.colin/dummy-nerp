package com.craft.nerp.invoice.domain;

import com.craft.nerp.shared.error.domain.ErrorKey;

public enum InvoiceErrorKey implements ErrorKey {
  UNKNOWN_RECIPIENT("invoices.unknown-recipient");

  private final String key;

  InvoiceErrorKey(String key) {
    this.key = key;
  }

  @Override
  public String get() {
    return key;
  }
}
