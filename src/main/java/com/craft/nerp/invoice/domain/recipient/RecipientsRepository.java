package com.craft.nerp.invoice.domain.recipient;

import java.util.Optional;

public interface RecipientsRepository {
  Optional<Recipient> get(RecipientId recipient);
}
