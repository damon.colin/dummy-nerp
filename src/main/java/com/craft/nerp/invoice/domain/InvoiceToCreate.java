package com.craft.nerp.invoice.domain;

import com.craft.nerp.invoice.domain.recipient.Recipient;
import com.craft.nerp.invoice.domain.recipient.RecipientId;
import com.craft.nerp.shared.error.domain.Assert;

public record InvoiceToCreate(RecipientId recipient, Lines lines) {
  public InvoiceToCreate {
    Assert.notNull("recipient", recipient);
    Assert.notNull("lines", lines);
  }

  public Invoice create(Recipient recipient) {
    Assert.notNull("recipient", recipient);

    return Invoice.builder().id(InvoiceId.newId()).recipient(recipient).lines(lines);
  }
}
