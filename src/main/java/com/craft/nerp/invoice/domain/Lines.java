package com.craft.nerp.invoice.domain;

import com.craft.nerp.shared.error.domain.Assert;
import java.util.Collection;
import java.util.stream.Stream;

public record Lines(Collection<Line> value) {
  public Lines {
    Assert.field("lines", value).notEmpty().noNullElement();
  }

  public Amount total() {
    return value().stream().map(Line::total).reduce(Amount.ZERO, Amount::add);
  }

  public Stream<Line> stream() {
    return value().stream();
  }
}
