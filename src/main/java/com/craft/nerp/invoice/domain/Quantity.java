package com.craft.nerp.invoice.domain;

import com.craft.nerp.shared.error.domain.Assert;
import java.math.BigDecimal;

public record Quantity(int value) {
  public Quantity {
    Assert.field("quantity", value).min(1);
  }

  public BigDecimal toBigDecimal() {
    return new BigDecimal(value());
  }
}
