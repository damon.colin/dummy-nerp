package com.craft.nerp.invoice.domain.recipient;

import com.craft.nerp.shared.error.domain.Assert;

public record RecipientName(RecipientFirstname firstname, RecipientLastname lastname) {
  public RecipientName {
    Assert.notNull("firstname", firstname);
    Assert.notNull("lastname", lastname);
  }
}
