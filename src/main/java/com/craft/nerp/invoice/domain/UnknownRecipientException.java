package com.craft.nerp.invoice.domain;

import com.craft.nerp.invoice.domain.recipient.RecipientId;
import com.craft.nerp.shared.error.domain.NerpException;

class UnknownRecipientException extends NerpException {

  protected UnknownRecipientException(RecipientId recipient) {
    super(badRequest(InvoiceErrorKey.UNKNOWN_RECIPIENT).message("Unknown recipient " + recipient));
  }
}
