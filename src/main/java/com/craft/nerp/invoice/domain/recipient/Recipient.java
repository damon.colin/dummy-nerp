package com.craft.nerp.invoice.domain.recipient;

import com.craft.nerp.invoice.domain.recipient.address.RecipientAddress;
import com.craft.nerp.shared.error.domain.Assert;

public record Recipient(RecipientName name, RecipientAddress address) {
  public Recipient {
    Assert.notNull("name", name);
    Assert.notNull("address", address);
  }

  public RecipientFirstname firstname() {
    return name().firstname();
  }

  public RecipientLastname lastname() {
    return name().lastname();
  }
}
