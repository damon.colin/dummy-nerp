package com.craft.nerp.customer.infrastructure.secondary;

import com.craft.nerp.customer.domain.Customer;
import com.craft.nerp.customer.domain.CustomerId;
import com.craft.nerp.customer.domain.CustomersRepository;
import com.craft.nerp.shared.error.domain.Assert;
import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
class DBCustomersRepository implements CustomersRepository {

  private final JpaCustomersRepository customers;

  public DBCustomersRepository(JpaCustomersRepository customers) {
    this.customers = customers;
  }

  @Override
  public void save(Customer customer) {
    Assert.notNull("customer", customer);

    customers.save(CustomerEntity.from(customer));
  }

  @Override
  public Optional<Customer> get(CustomerId customer) {
    Assert.notNull("customer", customer);

    return customers.findById(customer.value()).map(CustomerEntity::toDomain);
  }
}
