package com.craft.nerp.customer.infrastructure.secondary;

import com.craft.nerp.customer.domain.Customer;
import com.craft.nerp.customer.domain.CustomerFirstname;
import com.craft.nerp.customer.domain.CustomerId;
import com.craft.nerp.customer.domain.CustomerLastname;
import com.craft.nerp.customer.domain.CustomerName;
import com.craft.nerp.customer.domain.address.CustomerAddress;
import com.craft.nerp.shared.generation.domain.ExcludeFromGeneratedCodeCoverage;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "customer")
class CustomerEntity {

  @Id
  @Column(name = "id")
  private UUID id;

  @Column(name = "firstname")
  private String firstname;

  @Column(name = "lastname")
  private String lastname;

  @Column(name = "street")
  private String street;

  @Column(name = "postal_code")
  private String postalCode;

  @Column(name = "city")
  private String city;

  public static CustomerEntity from(Customer customer) {
    CustomerAddress address = customer.address();
    return new CustomerEntity()
      .id(customer.id().value())
      .firstname(customer.firstname().value())
      .lastname(customer.lastname().value())
      .street(address.street().value())
      .postalCode(address.postalCode().value())
      .city(address.city().value());
  }

  private CustomerEntity id(UUID id) {
    this.id = id;

    return this;
  }

  private CustomerEntity firstname(String firstname) {
    this.firstname = firstname;

    return this;
  }

  private CustomerEntity lastname(String lastname) {
    this.lastname = lastname;

    return this;
  }

  private CustomerEntity street(String street) {
    this.street = street;

    return this;
  }

  private CustomerEntity postalCode(String postalCode) {
    this.postalCode = postalCode;

    return this;
  }

  private CustomerEntity city(String city) {
    this.city = city;

    return this;
  }

  public Customer toDomain() {
    return Customer.builder().id(new CustomerId(id)).name(buildName()).address(buildAddress());
  }

  private CustomerName buildName() {
    return new CustomerName(new CustomerFirstname(firstname), new CustomerLastname(lastname));
  }

  private CustomerAddress buildAddress() {
    return CustomerAddress.builder().street(street).postalCode(postalCode).city(city);
  }

  @Override
  @ExcludeFromGeneratedCodeCoverage
  public int hashCode() {
    return new HashCodeBuilder().append(id).hashCode();
  }

  @Override
  @ExcludeFromGeneratedCodeCoverage
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    CustomerEntity other = (CustomerEntity) obj;
    return new EqualsBuilder().append(id, other.id).isEquals();
  }
}
