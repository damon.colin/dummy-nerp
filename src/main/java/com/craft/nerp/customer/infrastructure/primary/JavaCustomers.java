package com.craft.nerp.customer.infrastructure.primary;

import com.craft.nerp.customer.application.CustomersApplicationService;
import com.craft.nerp.customer.domain.Customer;
import com.craft.nerp.customer.domain.CustomerId;
import com.craft.nerp.customer.domain.address.CustomerAddress;
import com.craft.nerp.shared.customer_identity.domain.CustomerIdentity;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import org.springframework.stereotype.Service;

@Service
public class JavaCustomers {

  private final CustomersApplicationService customers;

  public JavaCustomers(CustomersApplicationService customers) {
    this.customers = customers;
  }

  public Optional<CustomerIdentity> get(UUID customerId) {
    return customers.get(new CustomerId(customerId)).map(toCustomerIdentity());
  }

  private Function<Customer, CustomerIdentity> toCustomerIdentity() {
    return customer -> {
      CustomerAddress address = customer.address();

      return CustomerIdentity
        .builder()
        .firstname(customer.firstname().value())
        .lastname(customer.lastname().value())
        .street(address.street().value())
        .postalCode(address.postalCode().value())
        .city(address.city().value());
    };
  }
}
