package com.craft.nerp.customer.domain.address;

import com.craft.nerp.shared.error.domain.Assert;

public record CustomerPostalCode(String value) {
  public CustomerPostalCode {
    Assert.field("postalCode", value).notBlank().maxLength(255);
  }
}
