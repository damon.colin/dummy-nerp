package com.craft.nerp.customer.domain;

import com.craft.nerp.customer.domain.address.CustomerAddress;
import com.craft.nerp.shared.error.domain.Assert;

public record CustomerToCreate(CustomerName name, CustomerAddress address) {
  public CustomerToCreate {
    Assert.notNull("name", name);
    Assert.notNull("address", address);
  }

  public Customer create() {
    return Customer.builder().id(CustomerId.newId()).name(name).address(address);
  }
}
