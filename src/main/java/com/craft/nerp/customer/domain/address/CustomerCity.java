package com.craft.nerp.customer.domain.address;

import com.craft.nerp.shared.error.domain.Assert;

public record CustomerCity(String value) {
  public CustomerCity {
    Assert.field("city", value).notBlank().maxLength(255);
  }
}
