package com.craft.nerp.customer.domain;

import com.craft.nerp.shared.error.domain.Assert;

public record CustomerName(CustomerFirstname firstname, CustomerLastname lastname) {
  public CustomerName {
    Assert.notNull("firstname", firstname);
    Assert.notNull("lastname", lastname);
  }
}
