package com.craft.nerp.customer.domain;

import com.craft.nerp.shared.error.domain.Assert;

public class CustomerCreator {

  private final CustomersRepository customers;

  public CustomerCreator(CustomersRepository customers) {
    Assert.notNull("customers", customers);

    this.customers = customers;
  }

  public Customer create(CustomerToCreate customerToCreate) {
    Assert.notNull("customerToCreate", customerToCreate);

    Customer customer = customerToCreate.create();
    customers.save(customer);

    return customer;
  }
}
