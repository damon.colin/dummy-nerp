package com.craft.nerp.customer.domain;

import com.craft.nerp.shared.error.domain.Assert;
import java.util.function.Function;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;

public record CustomerFirstname(String value) {
  private static final Pattern FIRSTNAME_PATTERN = Pattern.compile("(\\p{L}+)(\\P{L}*)");

  public CustomerFirstname(String value) {
    Assert.field("firstname", value).notBlank().maxLength(255);

    this.value = format(value);
  }

  private String format(String value) {
    return FIRSTNAME_PATTERN.matcher(value.trim()).results().map(toCapitalizedPart()).collect(Collectors.joining());
  }

  private Function<MatchResult, String> toCapitalizedPart() {
    return result -> StringUtils.capitalize(result.group(1)) + result.group(2);
  }
}
