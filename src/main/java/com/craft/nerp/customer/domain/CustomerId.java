package com.craft.nerp.customer.domain;

import com.craft.nerp.shared.error.domain.Assert;
import java.util.UUID;

public record CustomerId(UUID value) {
  public CustomerId {
    Assert.notNull("customerId", value);
  }

  public static CustomerId newId() {
    return new CustomerId(UUID.randomUUID());
  }
}
