package com.craft.nerp.customer.domain;

import com.craft.nerp.shared.error.domain.Assert;
import org.apache.commons.lang3.StringUtils;

public record CustomerLastname(String value) {
  public CustomerLastname(String value) {
    Assert.field("lastname", value).notBlank().maxLength(255);

    this.value = format(value);
  }

  private String format(String value) {
    return StringUtils.stripAccents(value).trim().toUpperCase();
  }
}
