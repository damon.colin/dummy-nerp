package com.craft.nerp.customer.domain;

import java.util.Optional;

public interface CustomersRepository {
  void save(Customer customer);

  Optional<Customer> get(CustomerId customer);
}
