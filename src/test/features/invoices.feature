Feature: Invoices management

  Background:
    Given I have customer
      | Firstname | Colin |
      | Lastname  | DAMON |

  Scenario: Should not get unknown invoice
    When I get invoice "d9555f89-4d73-4c80-a33c-daf167fa6f4a"
    Then Invoice should be unknown

  Scenario: Should get created invoice
    Given I create invoice for last customer
      | Label       | Quantity | Unit price |
      | First item  | 1        | 550.0      |
      | Second item | 2        | 1100.0     |

    When I get the created invoice
    Then I should have invoice recipient
      | Firstname            | Colin        |
      | Lastname             | DAMON        |
      | Address. Street      | 99 Rue d'ici |
      | Address. Postal code | 69000        |
      | Address. City        | Lyon         |
    And I should have invoice lines
      | Label       | Quantity | Unit price |
      | First item  | 1        | 550.0      |
      | Second item | 2        | 1100.0     |
    And I should have invoice total 2750.0
