package com.craft.nerp.customer.domain;

import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class CustomerIdTest {

  @Test
  void shouldGenerateId() {
    assertThat(CustomerId.newId()).isNotNull().isNotEqualTo(CustomerId.newId());
  }
}
