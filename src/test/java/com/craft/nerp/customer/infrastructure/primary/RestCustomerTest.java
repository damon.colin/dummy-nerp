package com.craft.nerp.customer.infrastructure.primary;

import static com.craft.nerp.customer.domain.CustomersFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.JsonHelper;
import com.craft.nerp.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class RestCustomerTest {

  @Test
  void shouldSerializeToJson() {
    assertThat(JsonHelper.writeAsString(RestCustomer.from(customer()))).isEqualTo(json());
  }

  private String json() {
    return """
    {\
    "id":"1e67ce12-a036-4362-a000-a926ea649ae2",\
    "firstname":"Colin",\
    "lastname":"DAMON",\
    "address":{"street":"99 Rue d'ici","postalCode":"69000","city":"Lyon"}\
    }\
    """;
  }
}
