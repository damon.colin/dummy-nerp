package com.craft.nerp.customer.infrastructure.secondary;

import static com.craft.nerp.customer.domain.CustomersFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class CustomerEntityTest {

  @Test
  void shouldConvertFromAndToDomain() {
    assertThat(CustomerEntity.from(customer()).toDomain()).usingRecursiveComparison().isEqualTo(customer());
  }
}
