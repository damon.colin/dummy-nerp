package com.craft.nerp.customer.infrastructure.primary;

import static com.craft.nerp.BeanValidationAssertions.*;
import static com.craft.nerp.customer.domain.CustomersFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.JsonHelper;
import com.craft.nerp.UnitTest;
import com.craft.nerp.customer.infrastructure.primary.RestAddress.RestAddressBuilder;
import com.craft.nerp.customer.infrastructure.primary.RestCustomerToCreate.RestCustomerToCreateBuilder;
import org.junit.jupiter.api.Test;

@UnitTest
class RestCustomerToCreateTest {

  private static final String CUSTOMER_CREATION =
    """
    {
      "firstname": "Colin",
      "lastname": "DAMON",
      "address": {
        "street": "99 Rue d'ici",
        "postalCode": "69000",
        "city": "Lyon"
      }
    }
    """;

  @Test
  void shouldDeserializeFromJson() {
    assertThat(JsonHelper.readFromJson(CUSTOMER_CREATION, RestCustomerToCreate.class).toDomain())
      .usingRecursiveComparison()
      .isEqualTo(customerToCreate());
  }

  @Test
  void shouldNotValidateEmptyCustomerToCreate() {
    assertThatBean(new RestCustomerToCreateBuilder().build())
      .hasInvalidProperty("firstname")
      .and()
      .hasInvalidProperty("lastname")
      .and()
      .hasInvalidProperty("address");
  }

  @Test
  void shouldNotValidateCustomerToCreateWithInvalidAddress() {
    assertThatBean(new RestCustomerToCreateBuilder().address(emptyAddress()).build()).hasInvalidProperty("address.street");
  }

  private RestAddress emptyAddress() {
    return new RestAddressBuilder().build();
  }
}
