package com.craft.nerp.invoice.infrastructure.secondary;

import static com.craft.nerp.JsonHelper.*;
import static com.craft.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@IntegrationTest
class JpaInvoicesRepositoryIT {

  @Autowired
  private JpaInvoicesRepository invoices;

  @Test
  void shouldSaveAndGetInvoice() {
    invoices.saveAndFlush(InvoiceEntity.from(jsonMapper(), invoice()));

    assertThat(invoices.findById(invoiceId().value()).get().toDomain(jsonMapper())).usingRecursiveComparison().isEqualTo(invoice());
  }
}
