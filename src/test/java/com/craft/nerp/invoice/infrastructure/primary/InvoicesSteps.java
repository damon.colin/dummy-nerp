package com.craft.nerp.invoice.infrastructure.primary;

import static com.craft.nerp.cucumber.rest.CucumberRestAssertions.*;

import com.craft.nerp.cucumber.rest.CucumberRestTemplate;
import com.craft.nerp.cucumber.rest.CucumberRestTestContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

public class InvoicesSteps {

  private static final String CUSTOMER_CREATION_TEMPLATE =
    """
    {
      "firstname": "{FIRSTNAME}",
      "lastname": "{LASTNAME}",
      "address": {
        "street": "{STREET}",
        "postalCode": "{POSTAL_CODE}",
        "city": "{CITY}"
      }
    }
    """;

  @Autowired
  private CucumberRestTemplate rest;

  private String lastCustomerId;

  @Given("I have customer")
  public void createCustomer(Map<String, String> customer) {
    rest.post("/api/customers", customerPayload(customer));

    lastCustomerId = (String) CucumberRestTestContext.getElement("$.id");
  }

  private String customerPayload(Map<String, String> customer) {
    return CUSTOMER_CREATION_TEMPLATE
      .replace("{FIRSTNAME}", customer.getOrDefault("Firstname", "Bob"))
      .replace("{LASTNAME}", customer.getOrDefault("Lastname", "MARTIN"))
      .replace("{STREET}", customer.getOrDefault("Street", "99 Rue d'ici"))
      .replace("{POSTAL_CODE}", customer.getOrDefault("Postal code", "69000"))
      .replace("{CITY}", customer.getOrDefault("City", "Lyon"));
  }

  @When("I create invoice for last customer")
  public void createInvoice(List<Map<String, String>> lines) {
    rest.post("/api/invoices", buildPayload(lines));
  }

  private String buildPayload(List<Map<String, String>> lines) {
    return lines.stream().map(toLinePayload()).collect(Collectors.joining(",", invoiceCreationHeader(), "]}"));
  }

  private String invoiceCreationHeader() {
    return new StringBuilder().append("{").append("\"recipient\":\"").append(lastCustomerId).append("\",\"lines\":[").toString();
  }

  private Function<Map<String, String>, String> toLinePayload() {
    return line ->
      new StringBuilder()
        .append("{\"label\": \"")
        .append(line.getOrDefault("Label", "Label"))
        .append("\",\"quantity\":")
        .append(line.getOrDefault("Quantity", "0"))
        .append(",\"unitPrice\":")
        .append(line.getOrDefault("Unit price", "550"))
        .append("}")
        .toString();
  }

  @When("I get the created invoice")
  public void getCreatedInvoice() {
    getInvoice((String) CucumberRestTestContext.getElement("$.id"));
  }

  @When("I get invoice {string}")
  public void getInvoice(String invoiceId) {
    rest.get("/api/invoices/" + invoiceId);
  }

  @Then("Invoice should be unknown")
  public void shouldHaveUnknownInvoice() {
    assertThatLastResponse().hasHttpStatus(HttpStatus.NOT_FOUND);
  }

  @Then("I should have invoice recipient")
  public void shouldHaveInvoiceRecipient(Map<String, String> recipient) {
    assertThatLastResponse().hasOkStatus().hasElement("$.recipient").containing(recipient);
  }

  @Then("I should have invoice lines")
  public void shouldHaveInvoiceLines(List<Map<String, String>> lines) {
    assertThatLastResponse().hasOkStatus().hasElement("$.lines").containingExactly(lines);
  }

  @Then("I should have invoice total {double}")
  public void shouldHaveInvoiceTotal(double total) {
    assertThatLastResponse().hasOkStatus().hasElement("$.total").withValue(total);
  }
}
