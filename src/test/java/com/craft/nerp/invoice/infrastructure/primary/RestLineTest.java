package com.craft.nerp.invoice.infrastructure.primary;

import static com.craft.nerp.BeanValidationAssertions.*;

import com.craft.nerp.UnitTest;
import com.craft.nerp.invoice.infrastructure.primary.RestLine.RestLineBuilder;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

@UnitTest
class RestLineTest {

  @Test
  void shouldNotValidateWithoutLabel() {
    assertThatBean(fullBuilder().label(null).build()).hasInvalidProperty("label");
  }

  @Test
  void shouldNotValidateWithBlankLabel() {
    assertThatBean(fullBuilder().label(" ").build()).hasInvalidProperty("label");
  }

  @Test
  void shouldNotValidateWithZeroQuantity() {
    assertThatBean(fullBuilder().quantity(0).build()).hasInvalidProperty("quantity");
  }

  @Test
  void shouldNotValidateWithNegativUnitPrice() {
    assertThatBean(fullBuilder().unitPrice(new BigDecimal("-1")).build()).hasInvalidProperty("unitPrice");
  }

  private RestLineBuilder fullBuilder() {
    return new RestLineBuilder().label("First item").quantity(1).unitPrice(new BigDecimal("42"));
  }
}
