package com.craft.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.UnitTest;
import java.math.BigDecimal;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@UnitTest
class AmountTest {

  @ParameterizedTest
  @MethodSource("amounts")
  void shouldScaleAtTwoDigits(String raw, String rounded) {
    assertThat(new Amount(new BigDecimal(raw))).isEqualTo(new Amount(new BigDecimal(rounded)));
  }

  static Stream<Arguments> amounts() {
    return Stream.of(
      Arguments.of("42.312", "42.31"),
      Arguments.of("42.315", "42.32"),
      Arguments.of("42.32", "42.32"),
      Arguments.of("42.3", "42.3")
    );
  }
}
