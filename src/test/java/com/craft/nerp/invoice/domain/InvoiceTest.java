package com.craft.nerp.invoice.domain;

import static com.craft.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.UnitTest;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

@UnitTest
class InvoiceTest {

  @Test
  void shouldGetTotal() {
    assertThat(invoice().total()).isEqualTo(new Amount(new BigDecimal("2750")));
  }
}
