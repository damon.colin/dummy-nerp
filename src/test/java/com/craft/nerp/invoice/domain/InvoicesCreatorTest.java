package com.craft.nerp.invoice.domain;

import static com.craft.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.nerp.UnitTest;
import com.craft.nerp.invoice.domain.recipient.RecipientsRepository;
import com.craft.nerp.shared.error.domain.ErrorStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@UnitTest
@ExtendWith(MockitoExtension.class)
class InvoicesCreatorTest {

  @Mock
  private RecipientsRepository recipients;

  @Mock
  private InvoicesRepository invoices;

  @InjectMocks
  private InvoicesCreator creator;

  @Test
  void shouldNotCreateInvoiceForUnknownRecipient() {
    assertThatThrownBy(() -> creator.create(invoiceToCreate()))
      .satisfies(throwable -> {
        UnknownRecipientException exception = (UnknownRecipientException) throwable;

        assertThat(exception.key()).isEqualTo(InvoiceErrorKey.UNKNOWN_RECIPIENT);
        assertThat(exception.status()).isEqualTo(ErrorStatus.BAD_REQUEST);
        assertThat(exception.getMessage()).contains(recipientId().value().toString());
      });
  }
}
