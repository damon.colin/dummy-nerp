package com.craft.nerp.shared.error.infrastructure.primary;

import com.craft.nerp.shared.error.domain.NerpException;

public final class NerpExceptionFactory {

  private NerpExceptionFactory() {}

  public static final NerpException buildEmptyException() {
    return NerpException.builder(null).build();
  }
}
